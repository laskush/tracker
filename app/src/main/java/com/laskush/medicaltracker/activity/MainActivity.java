package com.laskush.medicaltracker.activity;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.laskush.medicaltracker.FragmentReminder;
import com.laskush.medicaltracker.R;

public class MainActivity extends AppCompatActivity implements FragmentReminder.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentReminder rm = new FragmentReminder();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, rm).commit();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
